package mx.rulo4.spring.microservices.personservice.person.exceptions;

public class PersonNotFoundException extends RuntimeException {
  
  private static final String PERSON_NOT_FOUND_MESSAGE = "Doesn't exist person with id: %s";
  
  public PersonNotFoundException(Long id) {
    super(String.format(PERSON_NOT_FOUND_MESSAGE, id));
  }
}
