package mx.rulo4.spring.microservices.personservice.proxies;

import mx.rulo4.spring.microservices.personservice.person.address.Address;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

//@FeignClient(name = "address-service", url = "localhost:8101")
//@FeignClient(name = "address-service")
@FeignClient(name = "api-gateway")
@RibbonClient(name="address-service")
public interface AddressServiceProxy {
  
  @PostMapping("/address-service/addresses")
  ResponseEntity<Long> post(@Valid @RequestBody Address address);
}
