package mx.rulo4.spring.microservices.personservice.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import mx.rulo4.spring.microservices.personservice.person.address.Address;
import mx.rulo4.spring.microservices.personservice.person.bloodType.BloodType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Person {

  @Id @GeneratedValue private Long id;

  @NotNull(message = "First name is required")
  @Pattern(
      regexp = "[a-zA-Z]{3,100}",
      message = "First name must contain between 3 and 100 characters")
  private String firstName;

  @NotNull(message = "Last name is required")
  @Pattern(
      regexp = "[a-zA-Z]{3,100}",
      message = "Last name must contain between 3 and 100 characters")
  private String lastName;

  @NotNull(message = "Age is required")
  @Positive(message = "Age must be a positive number")
  @Digits(integer = 3, fraction = 0)
  private int age;

  @JoinColumn(name = "id")
  @OneToOne(targetEntity = BloodType.class, fetch = FetchType.LAZY)
  private BloodType bloodType;

  @Column(name = "blood_type_id")
  private Long bloodTypeId;

  @JoinColumn(name = "id")
  @OneToOne(targetEntity = Address.class, fetch = FetchType.LAZY)
  @Transient
  private Address address;

  @Column(name = "address_id")
  private Long addressId;
}
