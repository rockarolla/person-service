package mx.rulo4.spring.microservices.personservice.person.bloodType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BloodTypeRepository extends JpaRepository<BloodType, Long> {

}
