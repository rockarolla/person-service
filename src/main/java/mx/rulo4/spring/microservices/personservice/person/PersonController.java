package mx.rulo4.spring.microservices.personservice.person;

import mx.rulo4.spring.microservices.personservice.person.bloodType.BloodType;
import mx.rulo4.spring.microservices.personservice.person.bloodType.BloodTypeRepository;
import mx.rulo4.spring.microservices.personservice.person.exceptions.PersonNotFoundException;
import mx.rulo4.spring.microservices.personservice.proxies.AddressServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class PersonController {

  @Autowired private PersonRepository personRepository;
  @Autowired private BloodTypeRepository bloodTypeRepository;
  @Autowired private AddressServiceProxy addressServiceProxy;

  @GetMapping("/persons")
  public List<Person> getAll() {
    return this.personRepository.findAll();
  }

  @GetMapping("/persons/{id}")
  public Person get(@PathVariable Long id) {
    Optional<Person> personFound = this.personRepository.findById(id);

    if (!personFound.isPresent()) {
      throw new PersonNotFoundException(id);
    } else {
      return personFound.get();
    }
  }

  @PostMapping("/persons")
  public ResponseEntity<Person> post(@Valid @RequestBody Person person) {
    Optional<BloodType> personBloodType =
        this.bloodTypeRepository.findById(person.getBloodTypeId());
    if (personBloodType.isPresent()) {
      person.setBloodType(personBloodType.get());
      person.setBloodTypeId(personBloodType.get().getId());
    }

    ResponseEntity<Long> addressIdResponse =
        new RestTemplate()
            .postForEntity("http://localhost:8101/addresses", person.getAddress(), Long.class);
    person.setAddressId(addressIdResponse.getBody());

    Person personSaved = personRepository.save(person);

    URI createdLocation =
        ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(personSaved.getId())
            .toUri();

    return ResponseEntity.created(createdLocation).body(personSaved);
  }

  @PostMapping("/persons-feign")
  public ResponseEntity<Person> create(@Valid @RequestBody Person person) {
    Optional<BloodType> personBloodType =
        this.bloodTypeRepository.findById(person.getBloodTypeId());
    if (personBloodType.isPresent()) {
      person.setBloodType(personBloodType.get());
      person.setBloodTypeId(personBloodType.get().getId());
    }

    ResponseEntity<Long> addressIdResponse = addressServiceProxy.post(person.getAddress());
    person.setAddressId(addressIdResponse.getBody());
    Person personSaved = personRepository.save(person);

    URI createdLocation =
        ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(personSaved.getId())
            .toUri();

    return ResponseEntity.created(createdLocation)
        .header("Address-Server-Port", addressIdResponse.getHeaders().get("Server-Port").get(0))
        .body(personSaved);
  }

  @DeleteMapping("/persons/{id}")
  public void delete(@PathVariable Long id) {
    Optional<Person> personToDelete = this.personRepository.findById(id);

    if (!personToDelete.isPresent()) {
      throw new PersonNotFoundException(id);
    } else {
      this.personRepository.delete(personToDelete.get());
    }
  }
}
