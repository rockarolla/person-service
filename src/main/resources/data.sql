insert into blood_type (id, name) values (101, 'A+');
insert into blood_type (id, name) values (102, 'A-');
insert into blood_type (id, name) values (103, 'B+');
insert into blood_type (id, name) values (104, 'B-');
insert into blood_type (id, name) values (105, 'O+');
insert into blood_type (id, name) values (106, 'O-');
insert into blood_type (id, name) values (107, 'AB+');
insert into blood_type (id, name) values (108, 'AB-');

insert into person (id, first_name, last_name, age, blood_type_id, address_id) values (1001, 'Alberto', 'Avila', 30, 101, 101);
insert into person (id, first_name, last_name, age, blood_type_id, address_id) values (1002, 'Bertha', 'Bello', 20, 103, 101);
insert into person (id, first_name, last_name, age, blood_type_id, address_id) values (1003, 'Carlos', 'Cardozo', 15, 105, 101);
insert into person (id, first_name, last_name, age, blood_type_id, address_id) values (1004, 'Daniela', 'Domínguez', 20, 108, 101);
insert into person (id, first_name, last_name, age, blood_type_id, address_id) values (1005, 'Eladio', 'Enriquez', 40, 101, 101);
